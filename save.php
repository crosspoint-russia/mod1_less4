<?php

require __DIR__ . '/lib/functions.php'; // подключаем библиотеку функций

if (isset($_POST['newrecord']) && '' != $_POST['newrecord']) {

	//получаем записи в массив
	$gbRecords = getGuestBook();
	// получаем путь
	$gbPath = __DIR__ . '/lib/store.txt';

	// добавляем в массив новый элемент, предварительно прогнав пользовательские данные через функцию очистки
	$gbRecords[] = safeRecord($_POST['newrecord']);

		//пишем данные в файл
		$gbString = implode("\n", $gbRecords);
		file_put_contents ($gbPath, $gbString);

	// редирект на главную страницу гостевой
	header('Location: /mod1_less4.php');
} else {
	// редирект на главную страницу гостевой
	header('Location: /mod1_less4.php');	
}

?>