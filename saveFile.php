<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<?php

require __DIR__ . '/lib/functions.php'; // подключаем библиотеку функций

$uploadFolder = __DIR__ . '/uploads'; // папка для загрузки

if (0 == $_FILES['userfile']['error']) { // если нет ошибок при загрузке
    if ('image/png' == $_FILES['userfile']['type'] || 'image/jpeg' == $_FILES['userfile']['type']) { // и это подходящий нам тип файла

        // сохранение файла в папку с помощью кастомной функции
        // функция отличается от move_uploaded_file тем, что предварительно проверяет файл на существование
        // если файла нет, он сохраняется под пользовательским именем.
        // если файл существует, к пользовательскому имени файла добавляется окончание '_' . rand()
        // сохранение файла осуществляется вызовом move_uploaded_file внутри кастомной функции

        $uploadedFile = saveFile($uploadFolder . '/' . $_FILES['userfile']['name'], $_FILES['userfile']['tmp_name']);

        if (!is_null($uploadedFile)) { ?>

            <p><u>Uploaded file name:</u> <?php echo $uploadedFile; ?></p>
            <p><img src="/uploads/<?php echo $uploadedFile; ?>"></p>
            <p><a href="/mod1_less4.php">mod1_less4</a></p>

        <?php } else { ?>
            <p>Something went wrong</p>
        <?php }

    } else { ?>
        <p>Not allowed</p>
    <?php }
}

?>
</body>
</html>