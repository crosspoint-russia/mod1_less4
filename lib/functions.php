<?php

// функция возвращает массив из конкретного файла.
// дополнительно: если передаем аргумент, возвращает путь до этого файла.

function getGuestBook() {

	$gb = __DIR__ . '/store.txt'; // путь к файлу с записями гостевой кники

		if (is_readable($gb)) {
			return file($gb, FILE_IGNORE_NEW_LINES);
		} else {
			return null;
		}
	
}

// функция предварительной чистки строки перед сохранением в гостевой книге

function safeRecord($string) { // чистим строку от лишней информации
	$string = str_replace("\n", ' ', $string); // меняем символы переноса строки на пробелы
	$string = strip_tags($string); // убираем возможный html код
	$string = trim($string); // убираем  лишние символы из начала и конца строки
	return $string;
}

// функция для сохранения файла от пользователя с пользовательским именем и проверкой на дубликаты

function saveFile($destinationFile, $uploadedFile) {

    // проверяем файл на существование, если файл уже существует, дополняем имя рандомным значением
	if (file_exists($destinationFile)) {

		// разбиваем адрес в массив
		$exFile = pathinfo($destinationFile);

		// собираем новый путь с рандомной строкой в названии
		$newFilename = $exFile['dirname'] . '/' . $exFile['filename'] . '_' . rand() . '.' . $exFile['extension'];

		// возвращаем результат вызова самой себя.
		return saveFile($newFilename, $uploadedFile);

	} else {

		// если файла нет, то пишем и возвращаем имя файла. move_uploaded_file возвращает результатом bool значение.
		if (true == move_uploaded_file($uploadedFile, $destinationFile)) {
			// возвращаем только строку с сохраненным именем
			return pathinfo($destinationFile, PATHINFO_BASENAME);
		} else {
			return null;
		}

	}
}

?>